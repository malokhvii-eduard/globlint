FROM python:3.8-alpine
LABEL maintainer="malokhvii.ee@gmail.com"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

WORKDIR /globlint
COPY ["setup.py", "setup.cfg", "globlint.py", "LICENSE", "VERSION", "./"]
RUN python setup.py install \
  && rm -rf globlint \
  && mkdir /target \
  && mkdir /output \
  && mkdir /config

ENV GLOBLINT_TARGET "/target"
ENV GLOBLINT_OUTPUT "/output/globlint.json"
ENV GLOBLINT_CONFIG "/config/.globlintrc.yml"
ENV GLOBLINT_OPTS ""

VOLUME ["/target", "/output", "/config"]
ENTRYPOINT globlint $GLOBLINT_OPTS $GLOBLINT_TARGET $GLOBLINT_CONFIG $GLOBLINT_OUTPUT

ARG BUILD_VERSION
ARG BUILD_DATE
ARG VCS_REF

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date=$BUILD_DATE
LABEL org.label-schema.name="globlint"
LABEL org.label-schema.version=$BUILD_VERSION
LABEL org.label-schema.description="A tool for detecting inconsistencies in directories structure"
LABEL org.label-schema.usage="https://malokhvii-eduard.gitlab.io/globlint/#usage"
LABEL org.label-schema.url="https://malokhvii-eduard.gitlab.io/globlint"
LABEL org.label-schema.vcs-url="https://gitlab.com/malokhvii-eduard/globlint"
LABEL org.label-schema.vcs-ref=$VCS_REF
LABEL org.label-schema.vendor="Eduard Malokhvii"
