from pathlib import Path

from setuptools import setup  # type: ignore

VERSION = Path("VERSION").read_text().rstrip()

if __name__ == "__main__":
    setup(version=VERSION)
