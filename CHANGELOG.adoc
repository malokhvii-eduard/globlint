:linkcss:
:nofooter:
:sectanchors:
:docinfo: shared

ifdef::backend-html5[]
:toc: left
:toc-title:
:toclevels: 1
endif::[]

++++
<script>
document.title = "Glob Linter";
</script>
++++

[discrete]
== 📆 Changelog

All notable changes to this project will be documented in this file.

[#v1_7_4]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.7.4[1.7.4] - 2021-01-16

=== 🐛 Fixed

* Processing of `size` constraint with custom messages.
* Processing of `quantity` constraint with custom messages.

[#v1_7_3]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.7.3[1.7.3] - 2021-01-16

=== 🐛 Fixed

* Processing of `content` constraint for empty files.

[#v1_7_2]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.7.2[1.7.2] - 2021-01-16

=== 🐛 Fixed

* Processing of optional checks.

[#v1_7_1]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.7.1[1.7.1] - 2021-01-15

=== 🔨 Changed

* Decrease docker image size.

[#v1_7_0]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.7.0[1.7.0] - 2021-01-15

=== ✨ Added

* `quantity` constraint.

[#v1_6_0]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.6.0[1.6.0] - 2021-01-15

=== ✨ Added

* Custom messages for checks.

[#v1_5_0]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.5.0[1.5.0] - 2021-01-14

=== ✨ Added

* `--dry-run` option.

[#v1_4_0]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.4.0[1.4.0] - 2021-01-14

=== 🔨 Changed

* Replace acora with pure regular expressions.
* Remove unused dependencies.

=== 🐛 Fixed

* macOS compatibility.

=== 💥 Breaking Changes

* Rename keys in `keywords` constraint.
* Rename `keywords` constraint to `content`.
* Rename `size_limits` constraint to `size`.

[#v1_3_1]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.3.1[1.3.1] - 2021-01-12

=== 🔨 Changed

* Optimize error messages.
* Remove redundant errors messages.

[#v1_3_0]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.3.0[1.3.0] - 2021-01-11

=== ✨ Added

* `keywords` constraint.

[#v1_2_2]
==  📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.2.2[1.2.2] - 2021-01-11

=== 🐛 Fixed

* Disable SSL verification for config URL.
* Checking `size_limits` constraint.

[#v1_2_1]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.2.1[1.2.1] - 2021-01-11

=== 🐛 Fixed

* Dependencies mismatch.

[#v1_2_0]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.2.0[1.2.0] -  2021-01-10

=== ✨ Added

* Loading config by URL.

[#v1_1_0]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.1.0[1.1.0] - 2021-01-10

=== 🔨 Changed

* Optimize error messages.
* Remove redundant error messages.

=== 🐛 Fixed

* Processing of wildcards.

[#v1_0_0]
== 📦 https://gitlab.com/malokhvii-eduard/globlint/-/releases/v1.0.0[1.0.0] - 2020-12-11

=== ✨ Added

* Minimum viable product.
* Python package with linter.
* Docker image with linter.
